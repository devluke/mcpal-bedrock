# MCpal Bedrock - A lightweight Minecraft Server manager :)

## What does it do?

- Automatic backups at customizable times
- Extended commands (while fully supporting the native commands) like manual backups
- It's possible to automatically run other custom commands after each backup
- Logs from the console are saved when the server shuts down or restarts

## Install & Run:

1. Download "MCpal.jar"
2. Move it into your minecraft server directory
3. Open a console window and jump to the specific server directory (example: cd /home/Potato/minecraft_server/)
4. Now run "java -jar MCpal.jar b:PATH_TO_BACKUP_FOLDER
Example: java -jar MCpal.jar b:C:/Users/Potato/mc_backups
5. (Optional) A config file will be generated, which means that from now on you can call "java -jar MCpal.jar" after the first run. You no longer need to provide the other commands.

## Usage

In your terminal, where you actually run MCPal, just type:

- `backup`: To start a backup (including 10 seconds countdown)
- `stop`: To stop the server (including 10 seconds countdown)
- `istop`: To immediately stop the server
- `start`: To start the server

Other than that, the console will be the usual Minecraft server console. You can interact with it like with any other console.

### Custom backup times

By default, your server will automatically back up at 4AM and 4PM. However, this can be easily customized.

When you first run MCpal, a file named `times.txt` will be created. Put all the hours you want the server to back up in that file, separated by newlines.

For example, if you want the server to backup at 3AM, 7AM, 2PM, 7PM, and 10PM, the `times.txt` file would look like this:

```
3
7
14
19
22
```

## Feedback:

The original developer hasn't updated this project in years so you'll be much more likely to get a response from the maintainer of this fork:

- Luke#3416 on Discord
- [@ConsoleLogLuke](https://twitter.com/ConsoleLogLuke) on Twitter

**Original text:** Hit me up on telegram @codepleb for the shortest feedback loop. I cannot guarantee responses on other sources.
